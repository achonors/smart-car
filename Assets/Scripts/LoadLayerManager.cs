﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
 * 菊花层管理
 */

namespace Achonor {
    public class LoadLayerManager : SingleInstance<LoadLayerManager> {
        [SerializeField]
        private GameObject mUIMaskGo = null;

        //菊花层数
        private int mLoadingCount = 0;

        public int LoadingCount {
            get {
                return mLoadingCount;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            SetMaskActive(false);
        }

        public void AddLoad(int number = 1)
        {
            mLoadingCount += number;
            if (0 < mLoadingCount) {
                SetMaskActive(true);
            }
        }

        public void RemoveLoad(int number = 1)
        {
            mLoadingCount -= number;
            if (mLoadingCount < 0)
            {
                Debug.LogError("LoadLayerManager.RemoveLoad loadingCount = " + mLoadingCount);
                mLoadingCount = 0;
            }
            if (0 == mLoadingCount) {
                SetMaskActive(false);
            }
        }

        private void SetMaskActive(bool isShow) {
            if (null == mUIMaskGo) {
                return;
            }
            mUIMaskGo.SetActive(isShow);
        }
    }
}