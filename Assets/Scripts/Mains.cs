﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Achonor;

//主逻辑
public class Mains : SingleInstance <Mains> {
    [SerializeField]
    private MyJoystick mMyJoystick;

    [SerializeField]
    private Button mLEDChangeStatusBtn;

    [SerializeField]
    private InputField mAddressInput;

    [SerializeField]
    private Button mConnectBtn;

    [SerializeField]
    private UniversalMediaPlayer mMediaPlayer;
    /// <summary>
    /// LED灯状态
    /// </summary>
    private bool mLEDStatus = false;

    /// <summary>
    /// 最后发送方向的时间戳
    /// </summary>
    private long mLastSendDirectionTimestamp = 0;

    private Vector2Int mLastSendDirection = Vector2Int.zero; 

    private void Start() {


        //注册方向改变回调
        //mMyJoystick.RegisterIntResultChanged(ChangeDirection);
        Scheduler.CreateScheduler("Mains_ChangeDirection", 0, 0, 0.2f, () => {
            ChangeDirection(mMyJoystick.GetIntResult());
        });

        mLEDChangeStatusBtn.onClick.AddListener(() => {
            ChangeLEDStatus();
        });

        mConnectBtn.onClick.AddListener(()=> {
            if (string.IsNullOrEmpty(mAddressInput.text)) {
                return;
            }
            mMediaPlayer.Path = string.Format("http://{0}:81/stream", mAddressInput.text);
            mMediaPlayer.Play();
            Client.Instance.ConnectNetwork(mAddressInput.text, true, (isOK) => {
                Debug.Log("连接到小车结果：" + isOK);
            });
        });
    }

   /// <summary>
   /// 改变小车方向
   /// </summary>
   /// <param name="value"></param>
    public void ChangeDirection(Vector2Int value) {
        if (Function.GetLocaLTime() - mLastSendDirectionTimestamp < 180) {
            return;
        }
        if (mLastSendDirection.x == value.x && mLastSendDirection.y == value.y) {
            return;
        }
        byte[] bytes = new byte[] { 0x43, 0x05, 0x04, 0x00, 0x00};
        bytes[3] = (byte)(value.x + 100);
        bytes[4] = (byte)(value.y + 100);
        Debug.LogFormat("改变方向：{0}-{1}", bytes[3], bytes[4]);
        Client.Instance.Request(bytes);
        mLastSendDirectionTimestamp = Function.GetLocaLTime();
        mLastSendDirection = value;
    }

    /// <summary>
    /// LED灯开关
    /// </summary>
    public void ChangeLEDStatus() {
        mLEDStatus = !mLEDStatus;
        byte[] bytes = new byte[] { 0x43, 0x04, 0x03, 0x00 };
        bytes[3] = (byte)(mLEDStatus ? 0x01 : 0x00);
        Client.Instance.Request(bytes);

        mLEDChangeStatusBtn.GetComponentInChildren<Text>().text = (mLEDStatus ? "LED: 关" : "LED: 开");
    }
}
