﻿using Achonor;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum JoystickType {
    Fixed,      //固定式摇杆
    Floating,   //浮动式摇杆(根据点击屏幕的位置生成摇杆控制器)
    Dynamic     //动态摇杆(摇杆可以被动态拖拽)
}

public class MyJoystick : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler {
    [SerializeField]
    private Camera mCamera;

    public JoystickType joystickType = JoystickType.Fixed;
    public RectTransform background = null;
    public RectTransform handle = null;
    public float MoveThreshold = 0;

    private Canvas mCanvas;
    private RectTransform mBaseRect = null;

    private float deadZone = 0;
    public float DeadZone {
        get {
            return deadZone;
        }
        set {
            deadZone = Mathf.Abs(value);
        }
    }

    [SerializeField]
    private Vector2 input = Vector2.zero;
    [SerializeField]
    private Vector2Int intResult = Vector2Int.zero;
    private Vector2 center = new Vector2(0.5f, 0.5f);
    private Vector2 fixedPosition = Vector2.zero;

    /// <summary>
    /// 结果回调
    /// </summary>
    private Action<Vector2Int> mIntResultChangeCallback = null;

    protected virtual void Start() {
        mBaseRect = GetComponent<RectTransform>();
        mCanvas = GetComponentInParent<Canvas>();

        background.pivot = center;
        handle.anchorMin = center;
        handle.anchorMax = center;
        handle.pivot = center;
        handle.anchoredPosition = Vector2.zero;
        fixedPosition = background.anchoredPosition;
        SetMode();
    }

    public void SetMode() {
        if (joystickType == JoystickType.Fixed) {
            background.anchoredPosition = fixedPosition;
            background.gameObject.SetActive(true);
        } else
            background.gameObject.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData) {
        if (joystickType != JoystickType.Fixed) {
            background.anchoredPosition = ScreenPointToAnchoredPosition(eventData.position);
            background.gameObject.SetActive(true);
        }
        OnDrag(eventData);
    }

    public void OnDrag(PointerEventData eventData) {
        //将ui坐标中的background映射到屏幕中的实际坐标
        Vector2 position = Camera.main.WorldToScreenPoint(background.position);
        Vector2 radius = background.sizeDelta / 2;
        //将屏幕中的触点和background的距离映射到ui空间下实际的距离
        input = (eventData.position - position) / (radius * mCanvas.scaleFactor);
        //对输入进行限制
        HandleInput(input.magnitude, input.normalized, radius, mCamera);
        //实时计算handle的位置
        handle.anchoredPosition = input * radius;
        //计算车需要的值
        CalcIntResult();
    }

    public void OnPointerUp(PointerEventData eventData) {
        if (joystickType != JoystickType.Fixed) {
            background.gameObject.SetActive(false);
        }
        input = Vector2.zero;
        handle.anchoredPosition = Vector2.zero;
        //计算需要的值
        CalcIntResult();
    }

    public void HandleInput(float magnitude, Vector2 normalised, Vector2 radius, Camera cam) {
        if (joystickType == JoystickType.Dynamic && magnitude > MoveThreshold) {
            Vector2 difference = normalised * (magnitude - MoveThreshold) * radius;
            background.anchoredPosition += difference;
        }
        if (magnitude > deadZone) {
            if (magnitude > 1)
                input = normalised;
        } else {
            input = Vector2.zero;
        }
    }


    public void RegisterIntResultChanged(Action<Vector2Int> callback) {
        mIntResultChangeCallback = callback;
    }

    public Vector2Int GetIntResult() {
        return intResult;
    }

    private Vector2 ScreenPointToAnchoredPosition(Vector2 screenPosition) {
        Vector2 localPoint = Vector2.zero;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(mBaseRect, screenPosition, mCamera, out localPoint);
        return localPoint;
    }

    private void CalcIntResult() {
        Vector2Int result = new Vector2Int(0, 0);
        //计算右轮
        float lDistance = Vector2.Distance(input, new Vector2(1, 0));
        result.x = Mathf.Clamp((int)((100.0f / 0.414f) * (lDistance - 1)), -100, 100);
        if (input.y < 0) {
            result.x *= -1;
        }
        //计算右轮
        float rDistance = Vector2.Distance(input, new Vector2(-1, 0));
        result.y = Mathf.Clamp((int)((100.0f / 0.414f) * (rDistance - 1)), -100, 100);
        if (input.y < 0) {
            result.y *= -1;
        }

        if (result.x != intResult.x || result.y != intResult.y) {
            //变化了
            intResult = result;
            Function.CallCallback(mIntResultChangeCallback, intResult);
        }
    }
}