﻿using System;
using System.IO;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Achonor;

public delegate void RequestCallback(byte[] byteData);

public class Client : SingleInstance<Client> {
    //连接标识符
    private static int connectID = 0;
    //发送数据的数量
    private static int SendCount = 1;
    //数据发送队列
    private Queue<byte[]> SendQueue = new Queue<byte[]>();

    //发送数据间隔(帧)
    private static int SendInterval = 1;
    //接收数据间隔(帧)
    private static int RecvInterval = 1;
    //包头长度
    private static int LENGTH_HEAD = 2;
    //单次接收最大长度
    private static int DATA_MAX_LENGTH = 255;

    private void Update() {
        if (TCPSocket.IsConnect()) {
            //发送数据
            if (0 == Time.frameCount % SendInterval && 0 < SendQueue.Count) {
                TCPSocket.Send(SendQueue.Dequeue());
            }
            if (0 == Time.frameCount % RecvInterval) {
                onReceive();
            }
        }
    }
    private void OnApplicationQuit() {
        TCPSocket.Close();
    }


    public void ConnectNetwork(string address, bool needLoad = true, System.Action<bool> callback = null) {
        //获取服务器配置
        string port = "4567";
        Debug.Log("Client.ConnectNetwork Start Connect Server ip = " + address + " port = " + port);

        //连接
        if (true == needLoad) {
            LoadLayerManager.Instance.AddLoad();
        }
        TCPSocket.Connect(address, port, (isOK) => {
            if (true == needLoad) {
                LoadLayerManager.Instance.RemoveLoad();
            }
            callback(isOK);
        });
    }

    public void Request(byte[] protoByte) {
        //加入发送队列
        SendQueue.Enqueue(protoByte);
    }

    //当前接收的状态
    enum RecvState {
        idle = 0, //空闲状态
        head = 1, //接收包头
        body = 2, //接收数据体
        die = 3   //接收失败
    };
    delegate int FuncReceive(byte[] buffer, int len);
    //接收的数据
    private byte[] lastBuffer;
    //下次接受数据的长度
    private int receiveLen = 0;
    //当前接收状态
    private RecvState status = RecvState.idle;
    private void onReceive() {

        //接受数据的函数
        FuncReceive receive = delegate (byte[] buffer, int len) {
            if (DATA_MAX_LENGTH < len) {
                len = DATA_MAX_LENGTH;
            }
            int ret = TCPSocket.Receive(buffer, len);
            return ret;
        };
        //状态切换函数
        System.Action switch_idle = delegate () {
            status = RecvState.idle;
            receiveLen = 0;
        };
        System.Action switch_head = delegate () {
            status = RecvState.head;
            receiveLen = LENGTH_HEAD;
        };
        System.Action switch_die = delegate () {
            status = RecvState.die;
            receiveLen = 0;
        };
        System.Action<int> switch_body = delegate (int len) {
            status = RecvState.body;
            receiveLen = len;
        };

        //状态操作函数
        System.Action idle_func = delegate () {
            switch_head();
        };
        //接收包头
        System.Action head_func = delegate () {
            byte[] buffer = new byte[receiveLen];
            int ret = receive(buffer, receiveLen);
            if (ret <= 0 || 0x43 != buffer[0]) {
                return;
            }
            int bodyLen = buffer[1];
            if (DATA_MAX_LENGTH < bodyLen) {
                Debug.LogError("Client.onReceive body data is too long!");
            }
            switch_body(bodyLen - receiveLen);
        };
        //接受数据
        System.Action body_func = delegate () {
            byte[] buffer = new byte[receiveLen];
            int ret = receive(buffer, receiveLen);
            if (-1 == ret) {
                switch_die();
                return;
            }
            if (ret <= 0) {
                return;
            }
            if (null != lastBuffer && 0 < lastBuffer.Length) {
                //需要拼接数据
                lastBuffer = Function.MergeArray<byte>(lastBuffer, buffer);
            } else {
                lastBuffer = buffer;
            }
            if (ret < receiveLen) {
                //数据没有接收完
                switch_body(receiveLen - ret);
            } else {
                //数据接收完成
                try {
                    receiveData(lastBuffer);
                } finally {
                    lastBuffer = null;
                    switch_idle();
                }
            }
        };
        System.Action die_func = delegate () {
            //pass
        };

        //start
        switch (status) {
            case RecvState.idle:
                idle_func();
                break;
            case RecvState.head:
                head_func();
                break;
            case RecvState.body:
                body_func();
                break;
            case RecvState.die:
                head_func();
                break;
            default:
                idle_func();
                break;
        }
    }
    private void receiveData(byte[] data) {
        Request(new byte[4] {0x43, 0x04, 0x00, 0x01});
    }
}
